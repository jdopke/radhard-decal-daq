#include "DecalConfig.h"

unsigned char reverseNibble(unsigned char nibble) {
	unsigned char temp = nibble & 0xf;
	return (((temp & 0x8) >> 3) | ((temp & 0x4) >> 1) | ((temp & 0x2) << 1) | ((temp & 0x1) << 3));
}

unsigned char createConfig(unsigned int DAC, bool polarity, bool mask) {
	return (reverseNibble(DAC) | (polarity?0x10:0) | (mask?0x20:0));
}

unsigned int intreverseNibble(unsigned int nibble) {
  return (((nibble & 0x8) >> 3) | ((nibble & 0x4) >> 1) | ((nibble & 0x2) << 1) | ((nibble & 0x1) << 3));
}

// PixConfig::
PixConfig::PixConfig(unsigned char setVal) : pixConfig(setVal) {
//	std::cout << "New Pixel created with Config: " << int(setVal) << std::endl;
}
PixConfig::PixConfig(PixConfig &copyMe) {
//	std::cout << "Copying Pixel from Config: " << int(cfg.getPixConfig()) << std::endl;
	pixConfig = copyMe.pixConfig;
}
PixConfig::PixConfig(unsigned char calibDAC, bool polarity, bool mask) {
	pixConfig = reverseNibble(calibDAC);
	pixConfig |= (polarity?0x10:0);
	pixConfig |= (mask?0x20:0);
	pixConfig &= 0x3F; // remove any extra value coming from the 
}
PixConfig::~PixConfig() {}
bool PixConfig::isMasked() {return (pixConfig & 0x20);}
void PixConfig::setMasked(bool masked) {
	pixConfig = ((pixConfig & 0xDF) | (masked?0x20:0));
}
bool PixConfig::isNegative() {return ((pixConfig & 0x10) == 0);}
bool PixConfig::isPositive() {return (pixConfig & 0x10);}
void PixConfig::setPolarity(bool positive) {if (positive) {pixConfig = pixConfig | 0x10;} else {pixConfig = pixConfig & 0x2F;}}
unsigned char PixConfig::getDAC() {return (reverseNibble(pixConfig & 0xf));}
void PixConfig::setDAC(unsigned char value) {pixConfig = (pixConfig & 0x30) | (reverseNibble(value & 0xf));}
unsigned char PixConfig::getConfig() {return pixConfig;}
void PixConfig::setConfig(unsigned char config, unsigned char mask) { pixConfig = (pixConfig & ~(mask)) | (config & mask);}

// ColumnConfig
ColumnConfig::ColumnConfig(unsigned char defaultPixel) {
//	std::cout << "Generating Column Pixels with default setting: " << int(defaultPixel) << std::endl;
	for (int i=0;i<64;i++) {
//		std::cout << "Generating Pixel " << i << " with config " << int(defaultPixel) << std::endl;
		pix[i] = 0;
		pix[i] = new PixConfig(defaultPixel);
	}
//	std::cout << "All Pixels generated" << std::endl;
}
ColumnConfig::ColumnConfig(ColumnConfig &copyMe) {
	for(unsigned int i=0; i<64; i++)
		pix[i] = new PixConfig(*(copyMe.pix[i]));
}
ColumnConfig::~ColumnConfig() {
	for(unsigned int i=0; i<64; i++) {
		delete pix[i];
		pix[i] = 0;
	}
}
PixConfig *ColumnConfig::getPixConfig(unsigned int pixNumber) {
	if (pixNumber < 64) {return pix[pixNumber];}
	else {return (PixConfig*) NULL;}
}
void ColumnConfig::enable() {for(int i=0; i<64; i++) {pix[i]->setMasked(false);}}
void ColumnConfig::disable() {for(int i=0; i<64; i++) {pix[i]->setMasked(true);}}



// ChipConfig
ChipConfig::ChipConfig(unsigned char defaultPixel) {
	for (int i=0;i<64;i++) {
		columns[i] = 0;
		columns[i] = new ColumnConfig(defaultPixel);
	}
}
ChipConfig::ChipConfig(ChipConfig &copyMe) {
	for(unsigned int i=0; i<64; i++)
		columns[i] = new ColumnConfig(*(copyMe.columns[i])) ;
}
ChipConfig::~ChipConfig() {
	for(unsigned int i=0; i<64; i++) {
		delete columns[i];
		columns[i] = 0;
	}
}
ColumnConfig *ChipConfig::getColConfig(unsigned int colNumber) {return columns[colNumber];}
void ChipConfig::enable() {for(int i=0; i<64; i++) {columns[i]->enable();}}
void ChipConfig::disable() {for(int i=0; i<64; i++) {columns[i]->disable();}}
void ChipConfig::enableCol(unsigned int index) {columns[index]->enable();}
void ChipConfig::disableCol(unsigned int index) {columns[index]->disable();}
void ChipConfig::enableRow(unsigned int index) {for(int i=0; i<64; i++) {if (columns[i]->getPixConfig(index)) {columns[i]->getPixConfig(index)->setMasked(false);}}}
void ChipConfig::disableRow(unsigned int index) {for(int i=0; i<64; i++) {if (columns[i]->getPixConfig(index)) {columns[i]->getPixConfig(index)->setMasked(true);}}}
void ChipConfig::enablePix(unsigned int indexx, unsigned int indexy) {columns[indexx]->getPixConfig(indexy)->setMasked(false);}
void ChipConfig::disablePix(unsigned int indexx, unsigned int indexy) {columns[indexx]->getPixConfig(indexy)->setMasked(true);}
void ChipConfig::setDACPix(unsigned int indexx, unsigned int indexy, unsigned int DAC, bool positive, bool mask) {columns[indexx]->getPixConfig(indexy)->setPolarity(positive);
  columns[indexx]->getPixConfig(indexy)->setMasked(mask); 
  unsigned char value = createConfig(intreverseNibble(DAC), positive, mask);
  columns[indexx]->getPixConfig(indexy)->setDAC(value); // this doesnt change polarity or masking, only 4bit DAC
 }



// Helper function for byteswapping the ethernet data
uint16_t swapBytes(uint16_t val) {return (((val & 0xff00) >> 8) | ((val & 0xff) << 8));}


// RawData
// Helper Class for raw 64 Bit data from NEXYS
// following method expects to be able to access 4x16 bit at adress handed over...
RawData::RawData(uint16_t *data) {
	if(data) {
		for(unsigned int i=0; i<4; i++) {
			m_rawData[i] = data[i];
		}
	} else {
		for(unsigned int i=0; i<4; i++) {
			m_rawData[i] = 0;
		}
	}
}
RawData::RawData(const RawData &copyMe) {
	for(unsigned int i=0;i<4;i++)
		m_rawData[i] = copyMe.m_rawData[i];		
}
RawData::~RawData() {}
unsigned char RawData::getHit(unsigned int index) {
	if (index % 2) {
		return (unsigned char) (m_rawData[index/2] & 0xff);
	} else {
		return (unsigned char) ((m_rawData[index/2] & 0xff00) >> 8);
	}
}
bool RawData::isDecal() {return (bool) ((m_rawData[0] & 0xff00) == 0x9000);}

// StripHit
unsigned char StripHit::getSumHits() {
	unsigned char sum=0;
	for (unsigned int i=0; i<4; i++)
		sum+=getSingleHit(i);
	return sum;
}

// RawDataDecoder
RawDataDecoder::RawDataDecoder() {}
RawDataDecoder::RawDataDecoder(RawDataDecoder &copyMe) {
	for(unsigned int i=0;i<4;i++)
		currentData[i] = copyMe.currentData[i];
}
RawDataDecoder::~RawDataDecoder() {}
void RawDataDecoder::addData(uint16_t newVal) {
	currentData[0] = currentData[1];
	currentData[1] = currentData[2];
	currentData[2] = currentData[3];
	currentData[3] = newVal;
	return;
}
bool RawDataDecoder::isValidDecal() {
	return RawData(currentData).isDecal(); // Can one do this? I think so... although it looks evil.
}


// StreamData::
// Helper Class to fill data into and have an easier time to reconstruct later
StreamData::StreamData() : m_channel(0xFF) {
	for(unsigned int i=0;i<6;i++)
		m_words.push_back(0);
	m_data.clear();
}
StreamData::StreamData(const StreamData &copyMe) {
	m_channel = copyMe.m_channel;
	m_words = copyMe.m_words;
	m_data = copyMe.m_data;
}
StreamData::StreamData(uint16_t length, uint16_t *data, unsigned int requested) {
	m_data.clear();
	if(length < ((requested*4)+7)) {
		printf("Length of your data for filling StreamData seems corrupted\n");
	} else {
		m_channel = data[0] & 0xff;
		for (unsigned int i=0; i<6; i++) m_words.push_back(swapBytes(data[i+1]));
		for(unsigned int i=0; i<(requested*4); i++) {
			m_data.push_back(swapBytes(data[i+7]));
		}
	}
}
StreamData::~StreamData() {}
uint16_t StreamData::getWord(unsigned int index) {if (index < m_data.size()) {return m_data.at(index);} else {return 0;}}
uint16_t StreamData::getDataWord(unsigned int index) {
	unsigned int byteIndex = (index+index/7+1);
	if ( byteIndex < m_data.size()*2) {
		if (byteIndex % 2) {
			return (m_data.at(byteIndex/2) & 0xff);
		} else {
			return ((m_data.at(byteIndex/2) & 0xff00) >> 8);
		}
	} else {
		return 0xDEAD;
	}
}
uint16_t StreamData::getChannel() {return m_channel;}
uint16_t StreamData::getWords(unsigned int index) {if (index < 6) {return m_words[index];} else {return 0;}}
RawData StreamData::getRawData(unsigned int index) {
	if (index*4 < m_data.size()) {
		return RawData((uint16_t *)&(m_data[index*4]));
	} else {
		return RawData((uint16_t *) 0);
	}
}
unsigned int StreamData::getLength() {return m_data.size();}
unsigned int StreamData::getRawLength() {return m_data.size()/4;}
bool StreamData::operator< (const StreamData& str) const
{
    return (m_channel < str.m_channel);
}
// assume the object holds reusable storage, such as a heap-allocated buffer mArray
StreamData& StreamData::operator=(const StreamData& copyMe) {
	if (this != &copyMe) { // self-assignment check expected
		m_channel = copyMe.m_channel;
		m_words = copyMe.m_words;
		m_data = copyMe.m_data;
	}
	return *this;
}
void StreamData::dump(bool verbose) {
	if (verbose)
		printf("Dumping Data (%04lu words) for Channel # %02d\n", m_data.size(), m_channel);
	for(unsigned int i=0; i<( m_data.size() / 4); i++) {
		if (verbose)
			printf("Data Word %04d: (hex) %04x %04x %04x %04x\n", i, m_data[i*4], m_data[i*4+1], m_data[i*4+2], m_data[i*4+3]);
		else
			printf("%04x %04x %04x %04x\n", m_data[i*4], m_data[i*4+1], m_data[i*4+2], m_data[i*4+3]);
	}
}
