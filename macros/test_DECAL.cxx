// Follwing include lines have to commented in to operate with the compiled version of DECAL related Objects
#include "../stdll/DecalConfig.h"
#include "DecalMotherboard.h"
#include "TH2F.h"
#include <fstream>
#include <unistd.h>
#include <vector>
#include <string>

using namespace std;

void pon(bool strip=true){
  startUP(strip);
  setManualPhase(true, 8);
  setupAnalog();
}

void poff(){
  setManualPhase(true, 8);
  powerDOWN(0);
  //setVDAC(14, 3, 0.); //< VGuardRing
  //setVDAC(14, 2, 0.); //< VShaperCascN
  //setVDAC(14, 1, 0.); //< Threshold
  //setVDAC(14, 0, 0., true); //< Bias Voltage 
  //setAllCurrentDACs((float) 0.); 
}

void setDACperPix(unsigned int col, unsigned int row, unsigned int DAC, bool polarity, bool mask, ChipConfig *config) {
  if (config == NULL) {config = new ChipConfig();}
  config->setDACPix(col, row, DAC, polarity, mask);
}

void configureColumnOFF() {
	printf("Masking all Pixels\n");
	ColumnConfig *cols = new ColumnConfig(1);
	for (int i=0;i<64;i++) {
		std::cout<< "Generated Pixel " << i << " with configuration " << int(cols->getPixConfig(i)->getConfig()) << std::endl;
	}
	writeColumn(cols);
//	debugConfig(data);
	printf("That was a full Column Config\n");
}

void configureChipOFF() {
	printf("Masking all Pixels\n");
	ColumnConfig *cols = new ColumnConfig(1);
//	debugConfig(data);
	for (int i=0; i<63; i++) {
		writeColumn(cols);
	}
	printf("That was a full Chip Config\n");
}

// Write a threshold scan:
// Configure one Pixel on
// set Threshold over full range
// at each threshold setting run the following
// captureData with parameters for a large set of data, e.g. 64 data words to be returned.
// sum up numbers per channel, then histogram

void thresholdScan_PadMode(unsigned int nSteps, unsigned int row, unsigned int col, float range, float offset, bool resetCalib, int nStrobes, unsigned int config) {
	ChipConfig *cfg = new ChipConfig(config);
	writeChip(cfg);
	double step=(range/((double) nSteps-1))/2.0;
	TH2D *myHisto = new TH2D("ThresholdScan", "ThresholdScan", 4, -0.5, 3.5, nSteps, offset-step, offset+range+step);
	TH2D *myOverflowHisto = new TH2D("OverflowThresholdScan", "OverflowThresholdScan", 4, -0.5, 3.5, nSteps, offset-step, offset+range+step);
	std::vector<StreamData> toBeHistogrammed;
	operatePixels(true);
	calibratePixels(true);
	calibratePixels(false);
	operatePixels(false);
	for(unsigned int i=0; i<nSteps; i++) {
		setThreshold((float) ((range/(nSteps-1)*i)+offset));
		std::cout << "Threshold step: " << std::dec << i << " Value is: " << (float) ((range/(nSteps-1)*i)+offset);
		unsigned int nHits=0;
		unsigned int nOverflow=0;
		if (resetCalib) {
			operatePixels(true);
			calibratePixels(true);
			calibratePixels(false);
			operatePixels(false);
		}
		captureData(toBeHistogrammed, nStrobes, 16, true);
    //pad mode, data comes in on channels 3,7,11,15, overflow on 2,6,10,14
    	for (unsigned int k=0; k < toBeHistogrammed.size(); k++) {//loop over channels
    		if(k%4!=3){continue;} //ignore these channels, don't contain data in pad mode
			for(unsigned int m=0; m<((unsigned int) nStrobes*7); m++) {
				int hits=toBeHistogrammed[k].getRawData(m/7).getHit(m%7+1);
				int overflow=toBeHistogrammed[k-1].getRawData(m/7).getHit(m%7+1);
				myHisto->Fill((float)(3-(k-3)/4), (float) ((range/(nSteps-1)*i)+offset),hits);
				myOverflowHisto->Fill((float)(3-(k-3)/4), (float) ((range/(nSteps-1)*i)+offset),hits);
				nHits+=hits;
				nOverflow+=overflow;  	
			}
		}
		std::cout << " Hit Count was: " << nHits <<" Oveflow was: "<<nOverflow<< std::endl;
		toBeHistogrammed.clear();
	}
	myOverflowHisto->Draw("COLZ");
	myOverflowHisto->SaveAs("OverflowThresholdScan.root");
	myHisto->Draw("COLZ");
	myHisto->SaveAs("ThresholdScan.root");
}

void thresholdScan(unsigned int nSteps, unsigned int row, unsigned int col, float range, float offset, bool resetCalib, int nStrobes, unsigned int config, unsigned int delay) {
	ChipConfig *cfg = new ChipConfig(config);
	writeChip(cfg);
	double step=(range/((double) nSteps-1))/2.0;
	TH2D *myHisto = new TH2D("ThresholdScan", "ThresholdScan", 64, -0.5, 63.5, nSteps, offset-step, offset+range+step);
	std::vector<StreamData> toBeHistogrammed;
	operatePixels(true);
	calibratePixels(true);
	calibratePixels(false);
	operatePixels(false);
	for(unsigned int i=0; i<nSteps; i++) {
		setThreshold((float) ((range/(nSteps-1)*i)+offset));
		std::cout << "Threshold step: " << std::dec << i << " Value is: " << (float) ((range/(nSteps-1)*i)+offset);
		unsigned int nHits=0;
		captureData(toBeHistogrammed, nStrobes, 16, true, resetCalib, delay);
// And here is where the histogramming should happen over toBeHistogrammed[index]->getRawData(N[0-99])...
		for (unsigned int k=0; k < toBeHistogrammed.size(); k++) {
			for(unsigned int m=0; m<((unsigned int) nStrobes*7); m++) {
				StripHit *hit = new StripHit(toBeHistogrammed[k].getRawData(m/7).getHit(m%7+1));
				for (unsigned int p=0;p<4;p++) {
					if (hit->getSingleHit(p) > 0) {
						myHisto->Fill((float) (p+(15-k)*4), (float) ((range/(nSteps-1)*i)+offset), hit->getSingleHit(p));
						nHits+=hit->getSingleHit(p) ;
					}
				}
			}
		}
		std::cout << " Hit Count was: " << nHits << std::endl;
		toBeHistogrammed.clear();
	}
	myHisto->Draw("COLZ");
	myHisto->SaveAs("ThresholdScan.root");
	delete cfg;
	cfg = 0;
}


void testRegisterScan(unsigned int phase) {
	// Check capLength
	unsigned int capLength = 1;
	printf("Scanning test register values versus output\n");
	char histoname[200] = "";
	sprintf(histoname,"ThresholdScanRow%03d",phase);
	char filename[200] = "";
	sprintf(filename,"TestRegisterScanPhase%03d.root",phase);
	char histoTitle[200] = "";
	sprintf(histoTitle,"Testregister Scan Phase %2d;Column Number;Injection Number",phase);
	TH2D *myHisto = new TH2D(histoname, histoTitle, 64, -0.5, 63.5, 66, -0., 65.5);
	std::vector<StreamData> v;
	v.clear();
	setupDigital();
	setManualPhase(true, phase);
	reset_testVector();
	for(int nInjection=0; nInjection<66;nInjection++) {
		if (!nInjection) {
			inject_number(1,1);
		} else {
			inject_number(0,1);
		}
		captureData(v, capLength, 16, true, true, 0);
		for (unsigned int k=0; k < v.size(); k++) {
			for(unsigned int m=0; m<((unsigned int) capLength*7); m++) {
				StripHit *hit = new StripHit(v[k].getRawData(m/7).getHit(m%7+1));
				for (unsigned int p=0;p<4;p++) {
					if (hit->getSingleHit(p)) {
						myHisto->Fill((float) (p+(15-k)*4), (float) nInjection);
					}
				}
			}
		}
		v.clear();
	}
	myHisto->Draw("COLZ");
	myHisto->SaveAs(filename);
}

void testRegisterFineScan(unsigned int phase) {
	// Check capLength
	unsigned int capLength = 1;
	unsigned int nDelay = 32;
	printf("Scanning test register values versus output\n");
	char histoname[200] = "";
	sprintf(histoname,"TestRegisterScanFinePhase%03d",phase);
	char filename[200] = "";
	sprintf(filename,"TestRegisterScanFinePhase%03d.root",phase);
	char histoTitle[200] = "";
	sprintf(histoTitle,"Testregister Scan Fine at Coarse Phase %2d;Fine Phase Setting;Column",phase);
	TH2D *myHisto = new TH2D(histoname, histoTitle, nDelay, -0., ((float) nDelay - 0.5), 64, -0.5, 63.5);
	std::vector<StreamData> v;
	v.clear();
	setupDigital();
	setManualPhase(true, phase);
	reset_testVector();
	for(int k=0; k<16; k++) {
		inject_number(1,1);
		inject_number(0,1);
		inject_number(1,1);
		inject_number(0,1);
	}
	for(int delay=0; delay<nDelay;delay++) {
		for (int k=0;k<16;k++) {
			e->ConfigureVariable(10009,(((k&0x3f)<<8) | (0xff & delay)));
		}
		captureData(v, capLength, 16, true, true, 0);
		for(unsigned int m=0; m<((unsigned int) capLength*7); m++) {
			for (int k=0;k<16;k++) {
				StripHit *hit = new StripHit(v[k].getRawData(m/7).getHit(m%7+1));
				for (unsigned int p=0;p<4;p++) {
					if (hit->getSingleHit(p)) {
						myHisto->Fill((float) delay, (p+(15-k)*4));
					}
				}
			}
		}
		v.clear();
	}
	myHisto->Draw("COLZ");
	myHisto->SaveAs(filename);
}


void thresholdScanHalfCorrect(unsigned int nSteps, float range, float offset, bool resetCalib, int nStrobes, unsigned int config, unsigned int row=64, unsigned int col=0, unsigned int delay=0) {
	ColumnConfig *cfg = 0;
	if (row < 64) {
		cfg = new ColumnConfig(0);
		cfg->getPixConfig(row)->setConfig(config);
	} else {
		cfg = new ColumnConfig(config);
	}
	ColumnConfig *dummyCfg = new ColumnConfig(0);
	for (unsigned int i=0;i<63;i++) {
		if (i%2 != (col%2)) { // by picking col one can choose to configure odd or even columns
			writeColumn(dummyCfg, 2, true);
		} else {
			writeColumn(cfg);
		}
	}
	double step=(range/((double) nSteps-1))/2.0;
	TH2D *myHisto = new TH2D("ThresholdScan", "ThresholdScan", 64, -0.5, 63.5, nSteps, offset-step, offset+range+step);
	std::vector<StreamData> toBeHistogrammed;
	operatePixels(true);
	calibratePixels(true);
	calibratePixels(false);
	operatePixels(false);
	for(unsigned int i=0; i<nSteps; i++) {
		setThreshold((float) ((range/(nSteps-1)*i)+offset));
		std::cout << "Threshold step: " << std::dec << i << " Value is: " << (float) ((range/(nSteps-1)*i)+offset);
		unsigned int nHits=0;
		if (delay > 0) {
			captureData(toBeHistogrammed, nStrobes, 16, true, resetCalib, delay);
		} else {
			if (resetCalib) {
				operatePixels(true);
				calibratePixels(true);
				calibratePixels(false);
				operatePixels(false);
			}
			captureData(toBeHistogrammed, nStrobes, 16, true, resetCalib, delay);
		}
// And here is where the histogramming should happen over toBeHistogrammed[index]->getRawData(N[0-99])...
		for (unsigned int k=0; k < toBeHistogrammed.size(); k++) {
			for(unsigned int m=0; m<((unsigned int) nStrobes*7); m++) {
				StripHit *hit = new StripHit(toBeHistogrammed[k].getRawData(m/7).getHit(m%7+1));
				for (unsigned int p=0;p<4;p++) {
					if (hit->getSingleHit(p)) {
						myHisto->Fill((float) (p+(15-k)*4), (float) ((range/(nSteps-1)*i)+offset));
						nHits++;
					}
				}
			}
		}
		std::cout << " Hit Count was: " << nHits << std::endl;
		toBeHistogrammed.clear();
	}
	myHisto->Draw("COLZ");
	myHisto->SaveAs("ThresholdScan.root");
	delete cfg;
	cfg = 0;
}

void FirstColCfgTest(unsigned int nSteps, unsigned int row, float range, float offset, bool resetCalib, int nStrobes) {

	double step=(range/((double) nSteps-1))/2.0;
	TH2D *myHisto = new TH2D("ThresholdScan", "ThresholdScan", 32, -0.5, 31.5, nSteps, offset-step, offset+range+step);
	ChipConfig *cfg = new ChipConfig(0x00);

	for(unsigned int config=0; config<32; config++)
	{  
      //reset the chip cfg to 0x00
		writeChip(cfg);

      //write first col
		std::cout<<"Config is "<<config<<std::endl;
		ColumnConfig *colcfg = 0;
		if(config < 16) {
			colcfg = new ColumnConfig(reverseNibble(((15-config) & 0xf)));
		} else {
			colcfg = new ColumnConfig(reverseNibble((config & 0xf)) | 0x10);
		}
		writeColumn(colcfg);

		std::vector<StreamData> toBeHistogrammed;
		for(unsigned int i=0; i<nSteps; i++) {
			setThreshold((float) ((range/(nSteps-1)*i)+offset));
	//std::cout << "Threshold step: " << std::dec << i << " Value is: " << (float) ((range/(nSteps-1)*i)+offset);
			unsigned int nHits=0;
			if (resetCalib) {
				operatePixels(true);
				calibratePixels(true);
				calibratePixels(false);
				operatePixels(false);
			}
			captureData(toBeHistogrammed, nStrobes, 16, true);
	// And here is where the histogramming should happen over toBeHistogrammed[index]->getRawData(N[0-99])...
			for (unsigned int k=0; k < 1; k++) {
				for(unsigned int m=0; m<((unsigned int) nStrobes*7); m++) {
					StripHit *hit = new StripHit(toBeHistogrammed[k].getRawData(m/7).getHit(m%7+1));
					for (unsigned int p=3;p<4;p++) {
						if (hit->getSingleHit(p) && (float)(p+(15-k)*4)>62.5) {
							myHisto->Fill(config, (float) ((range/(nSteps-1)*i)+offset));
							nHits++;
						}
					}
					delete hit;
				}
			}
	//std::cout << " Hit Count was: " << nHits << std::endl;
			toBeHistogrammed.clear();
		}
		delete colcfg;
	}
	myHisto->Draw("COLZ");
	myHisto->SaveAs("ThresholdScan.root");
}

void thresholdScanConfig(unsigned int nSteps, float range, float offset, bool resetCalib, int nStrobes, ChipConfig *config) {
// Some examples of what could be done with a configuration
	writeChip(config);
	double step=(range/((double) nSteps-1))/2.0;
	TH2D *myHisto = new TH2D("ThresholdScan", "ThresholdScan", 64, -0.5, 63.5, nSteps, offset-step, offset+range+step);
	std::vector<StreamData> toBeHistogrammed;
	operatePixels(true);
	calibratePixels(true);
	calibratePixels(false);
	operatePixels(false);
	for(unsigned int i=0; i<nSteps; i++) {
		setThreshold((float) ((range/(nSteps-1)*i)+offset));
		std::cout << "Threshold step: " << std::dec << i << " Value is: " << (float) ((range/(nSteps-1)*i)+offset);
		unsigned int nHits=0;
		if (resetCalib) {
			operatePixels(true);
			calibratePixels(true);
			calibratePixels(false);
			operatePixels(false);
		}
		captureData(toBeHistogrammed, nStrobes, 16, true);
// And here is where the histogramming should happen over toBeHistogrammed[index]->getRawData(N[0-99])...
		for (unsigned int k=0; k < toBeHistogrammed.size(); k++) {
			for(unsigned int m=0; m<((unsigned int) nStrobes*7); m++) {
				StripHit *hit = new StripHit(toBeHistogrammed[k].getRawData(m/7).getHit(m%7+1));
				for (unsigned int p=0;p<4;p++) {
					if (hit->getSingleHit(p)) {
						myHisto->Fill((float) (p+(15-k)*4), (float) ((range/(nSteps-1)*i)+offset));
						nHits++;
					}
				}
			}
		}
		std::cout << " Hit Count was: " << nHits << std::endl;
		toBeHistogrammed.clear();
	}
	myHisto->Draw("COLZ");
	myHisto->SaveAs("ThresholdScan.root");
}

void thresholdScanRow(unsigned int nSteps, unsigned int row, float range, float offset, bool resetCalib, int nStrobes, ChipConfig *config/*, bool singlerow = true, bool update = true*/) {
// Some examples of what could be done with a configuration
	if (config == NULL) {config = new ChipConfig();}
	config->disable();
	if (row < 64) {
		config->enableRow(row);
	}
	writeChip(config);
	double step=(range/((double) nSteps-1))/2.0;
	char histoname[200] = "";
	sprintf(histoname,"ThresholdScanRow%02d",row);
	char filename[200] = "";
	//if(singlerow)
	sprintf(filename,"ThresholdScanRow%02d.root",row);
	//else
	//  sprintf(filename,"ThresholdScanRow.root");
	//char fileoption[200] = "";
	//if(update)
	//  sprintf(fileoption,"update");
	//else
	//  sprintf(fileoption,"recreate");
	TH2D *myHisto = new TH2D(histoname, histoname, 64, -0.5, 63.5, nSteps, offset-step, offset+range+step);
	std::vector<StreamData> toBeHistogrammed;
	operatePixels(true);
	calibratePixels(true);
	calibratePixels(false);
	operatePixels(false);
	for(unsigned int i=0; i<nSteps; i++) {
		setThreshold((float) ((range/(nSteps-1)*i)+offset));
		std::cout << "Threshold step: " << std::dec << i << " Value is: " << (float) ((range/(nSteps-1)*i)+offset);
		unsigned int nHits=0;
//		if (resetCalib) {
//			operatePixels(true);
//			calibratePixels(true);
//			calibratePixels(false);
//			operatePixels(false);
//		}
		captureData(toBeHistogrammed, nStrobes, 16, true, resetCalib, 0);
// And here is where the histogramming should happen over toBeHistogrammed[index]->getRawData(N[0-99])...
		for (unsigned int k=0; k < toBeHistogrammed.size(); k++) {
			for(unsigned int m=0; m<((unsigned int) nStrobes*7); m++) {
				StripHit *hit = new StripHit(toBeHistogrammed[k].getRawData(m/7).getHit(m%7+1));
				for (unsigned int p=0;p<4;p++) {
					if (hit->getSingleHit(p)) {
						myHisto->Fill((float) (p+(15-k)*4), (float) ((range/(nSteps-1)*i)+offset));
						nHits++;
					}
				}
				//delete hit;
			}
		}
		std::cout << " Hit Count was: " << nHits << std::endl;
		toBeHistogrammed.clear();
	}
	myHisto->Draw("COLZ");
	myHisto->SaveAs(filename);
	//TFile *myfile = new TFile(filename,fileoption);
	//myfile->cd();
	//myHisto->Write(myHisto->GetName(),TObject::kOverwrite);
	//myfile->Close();
	//delete myHisto;
	//delete myfile;
}


void thresholdScanChip(unsigned int nSteps, unsigned int row, float range, float offset, bool resetCalib, int nStrobes, ChipConfig *config) {
// Some examples of what could be done with a configuration
	if (config == NULL) {config = new ChipConfig();}
	config->disable();
	if (row < 64) {
		config->enableRow(row);
	}
	writeChip(config);
	double step=(range/((double) nSteps-1))/2.0;
	char histoname[200] = "";
	sprintf(histoname,"ThresholdScanRow%02d",row);
	char filename[200] = "";
	sprintf(filename,"ThresholdScanRow%02d.root",row);
	TH2D *myHisto = new TH2D(histoname, histoname, 64, -0.5, 63.5, nSteps, offset-step, offset+range+step);
	std::vector<StreamData> toBeHistogrammed;
	operatePixels(true);
	calibratePixels(true);
	calibratePixels(false);
	operatePixels(false);
	for(unsigned int i=0; i<nSteps; i++) {
		setThreshold((float) ((range/(nSteps-1)*i)+offset));
		std::cout << "Threshold step: " << std::dec << i << " Value is: " << (float) ((range/(nSteps-1)*i)+offset);
		unsigned int nHits=0;
//		if (resetCalib) {
//			operatePixels(true);
//			calibratePixels(true);
//			calibratePixels(false);
//			operatePixels(false);
//		}
		captureData(toBeHistogrammed, nStrobes, 16, true, resetCalib, 0);
// And here is where the histogramming should happen over toBeHistogrammed[index]->getRawData(N[0-99])...
		for (unsigned int k=0; k < toBeHistogrammed.size(); k++) {
			for(unsigned int m=0; m<((unsigned int) nStrobes*7); m++) {
				StripHit *hit = new StripHit(toBeHistogrammed[k].getRawData(m/7).getHit(m%7+1));
				for (unsigned int p=0;p<4;p++) {
					if (hit->getSingleHit(p)) {
						myHisto->Fill((float) (p+(15-k)*4), (float) ((range/(nSteps-1)*i)+offset));
						nHits++;
					}
				}
			}
		}
		std::cout << " Hit Count was: " << nHits << std::endl;
		toBeHistogrammed.clear();
	}
	myHisto->Draw("COLZ");
	myHisto->SaveAs(filename);
}

void runStripCheck() {
	std::vector<StreamData> v;
	unsigned int words=1;
	unsigned int nChannels=18;
	unsigned char i=0;
	while (true) {
		StripHit fill = StripHit(i);
		std::vector<unsigned int> values;
		for (unsigned int k=0; k<64; k++) {
			values.push_back(fill.getSingleHit((3-(k%4))));
		}
		inject_numbers(values);
		captureData(v, words, nChannels);
		for (unsigned int k=0; k<16; k++) {
			if (StripHit(v[k].getRawData(0).getHit(1)) != fill) {
				printf("%03d: Error in returned data, Channel %x, Data is %02x, Expected %02x\n", i, v[k].getChannel(), v[k].getRawData(0).getHit(1), fill.getData());
			}
		}
		v.clear();
		if(i==255) break;
		i++;
	}
}

void runPadCheck() {
	std::vector<StreamData> v;
	unsigned int words=1;
	unsigned int nChannels=18;
	unsigned int i=0;
	std::vector<unsigned int> sum2be;
	sum2be.clear();
//	printf("Starting PadCheck...\n");
	while (true) {
//		printf("In Loop, counter %d\n", i);
		unsigned int temp = i;
		sum2be.clear();
		for (unsigned int k=0; k<16; k++) {
			if (temp < 31) {
				sum2be.push_back(temp);
			} else {
				sum2be.push_back(31);
				temp -= 31;
			}
		}
		for (unsigned int blocks = 0; blocks < 4; blocks++) {
			inject_numbers(sum2be);
//			printf("Block Number %d is being configured\n", blocks);
		}
		unsigned int sum = 0;
		unsigned int overflow = 0;
		for (unsigned int index=0; index<sum2be.size(); index++) {
			sum += (sum2be.at(index) & 0xf);
			overflow += ((sum2be.at(index) & 0x10) >> 4);
		}
		PadHit fill = PadHit(sum, overflow);
		captureData(v, words, nChannels);
		for (unsigned int k=2; k<16; k+=4) {
			if (PadHit(v[k+1].getRawData(0).getHit(1), v[k].getRawData(0).getHit(1)) != fill) {
				printf("%03d: Error in returned data, Channel %x, Sum is %02x, Channel %x, Overflow is %02x, Expected %02x and %02x\n", i, v[k+1].getChannel(), v[k+1].getRawData(0).getHit(1), v[k].getChannel(), v[k].getRawData(0).getHit(1), fill.getSum(), fill.getOverflow());
			}

		}
		v.clear();
		i++;
		if(i>31*16) break;
	}
}

void resetChip(bool enable) {
	ChipConfig *cfg = new ChipConfig(0);
// Some examples of what could be done with a configuration
//	cfg->disable(); // disable all pixels
//	cfg->enableRow(row); // enable one row (as per parameters)
	if (enable) {
		cfg->enable();
	} else {
		cfg->disable();
	}
	writeChip(cfg);
}

void DECAL_UseHisto(uint32_t delay, int dg_rates, bool linkid_mode, int recordMode=3) {

  //DG Rates:
  //  0-4  : 16MHz   8MHz   4MHz   2MHz   1MHz
  //  5-9  : 1600kHz 800kHz 400kHz 200kHz 100kHz 
  // 10-14 : 160kHz  80kHz  40kHz  20kHz  10kHz   
  //   15  : 16kHz
        
  int rates_khz[16] = {
    16000, 8000, 4000, 2000, 1000,
    1600,  800,  400,  200,  100,
    160,   80,   40,   20,   10,
    16};
  
  unsigned int modeSwitch = ((recordMode & 0x3) << 8) | 0xf0ff;

  // Init
  e->ConfigureVariable(10019, 0x1000); // Disable Trigger-Data packets
  e->ConfigureVariable(10023, 0x0000);

  // Modes decode
  if (linkid_mode) {
    printf("LinkID mode: Setting counters = 0xdeca1 + linkid(8b) + sub-link(4b).\n");
    e->ConfigureVariable(10023, 0x0010);
  }
  else if (dg_rates > 0xffff) {
    printf("DG rates value too high, aborting!\n");
    return;  
  }
  else if (dg_rates >= 0) {
    printf("DataGen mode, using DG0, sub-link rates (0-3) = ");
    printf("%1x(%dkHz), %1x(%dkHz), %1x(%dkHz), %1x(%dkHz)\n",
	   dg_rates&0xf,       rates_khz[dg_rates&0xf],
	   (dg_rates>>4)&0xf,  rates_khz[(dg_rates>>4)&0xf],
	   (dg_rates>>8)&0xf,  rates_khz[(dg_rates>>8)&0xf],
	   (dg_rates>>12)&0xf, rates_khz[(dg_rates>>12)&0xf]);
    e->ConfigureVariable(10036, dg_rates);
    e->ConfigureVariable(10037, dg_rates);      //for the future
    streamConfigWrite(0x000f, 0, 0x0008, 16*2); //Data source = DG0
  }
  else
    //streamConfigWrite(0xffff,0,(0x8311 & modeSwitch), 16*2);
    streamConfigWrite(0x88ff, 0, 0x8309, 16*2);

  uint16_t sb[300]; // status block buffer
  //Readback status block2 - using extended status block feature
  //Matt_GetStatusBlock2(sb, 2, delay, 0); 

  static uint16_t seqnum = 0x7000;
  uint16_t ocdata[15];
  int i=0;
  ocdata[i++] = 0x2;
  ocdata[i++] = (delay >> 16) & 0xffff;
  ocdata[i++] = delay & 0xffff;
    
  uint16_t length = e->HsioSendReceiveOpcode(0x19, seqnum++, i, ocdata, 128+3, sb);
  if(length > 0xf000) printf("GetStatusBlock2: ERROR: No response\n");
  else {
    for (int n=0; n<(128+3); n++) sb[n]=((sb[n]&0xff)<< 8)|((sb[n]&0xff00)>>8);

    printf("Hdr: %04x %04x %04x\n", sb[0], sb[1], sb[2]);
    for (int n=3; n<(128+3); n++) {
      if ((n-3)%8 == 0) printf("%3d: ",(n-3));
      printf("%04x ", sb[n]);
      if ((n-3)%8 == 7) printf("\n");
    }
    printf("\n");
  }

  streamConfigWrite(0x000f, 0, 0, 16*2);
}


std::vector<int> captureCounters(unsigned int capLength, unsigned int nChannels, unsigned int delay) {
	std::vector<int> returnCounts;
	returnCounts.clear();
	// Check capLength
	if (capLength < 1) {
		printf("Not capturing nothin', screw you!\n");
		return returnCounts;
	}


	//printf("Capturing Data on %d streams\n", nChannels);

	e->HsioFlush();
	Matt_SendStreamCommand(2,0,32); // clear counters 
	e->ConfigureVariable(10019, 0x1000); // Disable Trigger-Data packets
	
	e->ConfigureVariable(10036,0x0000); // enable DG
	streamConfigWrite(0xffff, 0, 0x8000, nChannels*2); //No more capture?
	e->ConfigureVariable(10023, 0x0000);
	e->ConfigureVariable(10032, 0x200a); // VarLen + Shortest TO
	e->ConfigureVariable(10023, 0x880); // set trig = capt start

	e->ConfigureVariable(10035, (delay>>16)&0xffff);
	e->ConfigureVariable(10034, delay&0xffff);

	//Set count-timer period (12.5ns steps)
	e->ConfigureVariable(10039, 0);
	e->ConfigureVariable(10038, (2000&0xffff)); //25us

	for(unsigned int i=0; i<64;i++) returnCounts.push_back(0);
	static uint16_t seqnum = 0x7000;
	uint16_t sb[300];
	uint16_t data[15];
	uint16_t opcode = 0x19;


	uint16_t rxsize   = 128+3;  
	data[0] = 0;

	while (capLength > 2000) {
		resetPixels(0);
		if (delay > 0xffff) e->Sleep(delay/80000);
		capLength-=2000;
	}
	e->ConfigureVariable(10038, (capLength & 0xffff));
	resetPixels(0);
	if (delay > 0xffff) e->Sleep(delay/80000);

	int i=0;
	data[i++] = 0x2;
	data[i++] = 0;
	data[i++] = 0;


	uint16_t length = e->HsioSendReceiveOpcode(opcode, seqnum++, i, data, rxsize, sb);
	if(length > 0xf000) { printf("GetStatusBlock: ERROR: No response\n"); return returnCounts; }

	for (int n=0; n<rxsize; n++) sb[n]=((sb[n]&0xff)<< 8)|((sb[n]&0xff00)>>8);

	for (int n=0; n<64; n++) {
		returnCounts[n]+= (sb[2*n+3] | (sb[2*n+4]<<16));
	}
	streamConfigWriteAll(0x000f, 0);
	return returnCounts;
}

// Give a row parameter bigger 63 to get all rows scanned as per the given chip config!
void counterThresholdScanRow(unsigned int nSteps, unsigned int row, float range, float offset, bool resetCalib, int nStrobes, unsigned int nsleep, ChipConfig *config, std::string nameoffile="", std::string nameofhisto="", bool singlerow = true, bool update = false) {
// Some examples of what could be done with a configuration
	if (config == NULL) {config = new ChipConfig();}
	if (row < 64) {
		config->disable();
		config->enableRow(row);
	}
	writeChip(config);
	double step=(range/((double) nSteps-1))/2.0;
	char histoname[200] = "";
	if(!(nameofhisto==""))
	  sprintf(histoname,"%s",nameofhisto.c_str());
	else
	  sprintf(histoname,"counterThresholdScanRow%02d",row);
	//char filename[200] = "";
	//sprintf(filename,"counterThresholdScanRow%02d.root",row);
	char filename[200] = "";
	if(!(nameoffile==""))
	  sprintf(filename,"%s",nameoffile.c_str());
	else if(singlerow)
	  sprintf(filename,"counterThresholdScanRow%02d.root",row);
	else
	  sprintf(filename,"counterThresholdScanMultipleRows.root");
	char fileoption[200] = "";
	if(update)
	  sprintf(fileoption,"update");
	else
	  sprintf(fileoption,"recreate");
	TH2D *myHisto = new TH2D(histoname, histoname, 64, -0.5, 63.5, nSteps, offset-step, offset+range+step);
	std::vector<StreamData> toBeHistogrammed;
	for(unsigned int i=0; i<nSteps; i++) {
		resetPixels(0);
		setThreshold((float) ((range/(nSteps-1)*i)+offset));
		std::cout << "Threshold step: " << std::dec << i << " Value is: " << (float) ((range/(nSteps-1)*i)+offset) << " Sleeping " << nsleep << "BCs" << std::endl;
		unsigned int nHits=0;

		std::vector<int> temp = captureCounters(nStrobes, 16, nsleep);
// And here is where the histogramming should happen over toBeHistogrammed[index]->getRawData(N[0-99])...
		for(int index=0; index<64; index++) {
			myHisto->Fill((float) (index), (float) ((range/(nSteps-1)*i)+offset), temp[63-index]);
			nHits += temp[63-index];
		}
		std::cout << " Hit Count was: " << nHits << std::endl;
		toBeHistogrammed.clear();
	}
	//myHisto->Draw("COLZ");
	//myHisto->SaveAs(filename);
	TFile *myfile = new TFile(filename,fileoption);
	myfile->cd();
	myHisto->Write(myHisto->GetName(),TObject::kOverwrite);
	myfile->Close();
	std::cout << " Saved histo " << myHisto->GetName() << " to file " << myfile->GetName() << std::endl;
	delete myHisto;
	delete myfile;
}

// Scan all rows inside vector "rows"
void counterThresholdScanMultipleRows(unsigned int nSteps, std::vector<unsigned int> rows, float range, float offset, bool resetCalib, int nStrobes, unsigned int nsleep, ChipConfig *config, std::string nameoffile="", std::string nameofhisto="") {
  for(unsigned int r = 0; r<rows.size();++r){
    char filename[200] = "";
    sprintf(filename,"counterThresholdScanRow%02d_%02d.root",rows[r],r);
    counterThresholdScanRow(nSteps, rows[r], range, offset, resetCalib, nStrobes, nsleep, config, filename, nameofhisto, false, true);
  }
}

// repeat a measurement of rows in a given series multiple times, to investigate temperature effects
void cTScanRepetition(unsigned int nSteps, unsigned int NrepExpo, std::vector<unsigned int> repeatme, float range, float offset, bool resetCalib, int nStrobes, unsigned int nsleep, ChipConfig *config, std::string nameoffile="", std::string nameofhisto=""){
  for(unsigned int i = 0; i<NrepExpo; ++i)
    repeatme.insert(repeatme.end(), repeatme.begin(), repeatme.end()); // appends it to itself and grows exponentially      
  counterThresholdScanMultipleRows(nSteps, repeatme, range, offset, resetCalib, nStrobes, nsleep, config, nameoffile, nameofhisto);
}

void counterThresholdScanAllRows(unsigned int nSteps, float range, float offset, bool resetCalib, int nStrobes, unsigned int nsleep, ChipConfig *config, std::string nameoffile="", std::string nameofhisto="") {
  for(unsigned int r = 0; r<64;++r)
    counterThresholdScanRow(nSteps, r, range, offset, resetCalib, nStrobes, nsleep, config, nameoffile, nameofhisto, false, true);
}

void thresholdScanAllRows(unsigned int nSteps, float range, float offset, bool resetCalib, int nStrobes, ChipConfig *config) {
  for(unsigned int r = 0; r<64;++r)
    thresholdScanRow(nSteps, r, range, offset, resetCalib, nStrobes, config);
}

void delayThresholdScanRow(unsigned int nSteps, unsigned int row, unsigned int col, float range, float offset, bool resetCalib, int nStrobes, unsigned int sleeprange, unsigned int sleepsteps, ChipConfig *config) {
// Some examples of what could be done with a configuration
	if (config == NULL) {config = new ChipConfig();}
	config->disable();
	if (row < 64) {
		config->enableRow(row);
	}
	writeChip(config);
	double step=(range/((double) nSteps-1))/2.0;
	char histoname[200] = "";
	sprintf(histoname,"counterThresholdScanRow%02d",row);
	char filename[200] = "";
	sprintf(filename,"counterThresholdScanRow%02d.root",row);
	TH2D *myHisto = new TH2D(histoname, histoname, sleepsteps, -0.5, sleeprange-0.5, nSteps, offset-step, offset+range+step);
	for(unsigned int i=0; i<nSteps; i++) {
		for(unsigned int k=0; k<sleepsteps; k++) {
			setThreshold((float) ((range/(nSteps-1)*i)+offset));
			resetPixels(0);
			int sleeptime=int(float(sleeprange)/float(sleepsteps)*float(k));
			std::cout << "Threshold step: " << std::dec << i << " Value is: " << (float) ((range/(nSteps-1)*i)+offset) << " Sleeping " << sleeptime/2.0 << "BCs" << std::endl;
			std::vector<int> temp = captureCounters(nStrobes, 16, sleeptime);
	// And here is where the histogramming should happen over toBeHistogrammed[index]->getRawData(N[0-99])...
			myHisto->Fill((float) (sleeptime), (float) ((range/(nSteps-1)*i)+offset), temp[63-col]);
		}
	}
	myHisto->Draw("COLZ");
	myHisto->SaveAs(filename);
}


void TuningScanRow(unsigned int nSteps, unsigned int row, float range, float offset, bool resetCalib, int nStrobes, unsigned int nsleep, ChipConfig *config, std::string nameoffile="",std::string nameofhisto="", bool singlerow = true, bool update = true, bool termoutput = false) {
// Some examples of what could be done with a configuration
	if (config == NULL) {config = new ChipConfig();}
	config->disable();
	if (row < 64) {
		config->enableRow(row);
	}
	for( unsigned int pol = 0; pol<2; pol++){
	  bool polarity = !!pol; // for int to bool conversion
	  for(unsigned int DAC = 0; DAC<16; DAC++){
	    // loop over all cols to set all to the same DAC
	    for(unsigned int col = 0; col<64; col++){
	      setDACperPix(col, row, DAC, polarity, false, config);}
	    // FROM HERE SAME AS counterthresholdscanrow...
	    writeChip(config);
	    double step=(range/((double) nSteps-1))/2.0;
	    char histoname[200] = "";
	    if(!(nameofhisto==""))
	      sprintf(histoname,"%s",nameofhisto.c_str());
	    else 
	      sprintf(histoname,"TuningScanRow%02dDAC%02dPol%d",row, DAC, pol);
	    //char filename[200] = "";
	    //sprintf(filename,"TuningScanRow%02d.root",row);
	    char filename[200] = "";
	    if(!(nameoffile==""))
	      sprintf(filename,"%s",nameoffile.c_str());
	    else if(singlerow)
	      sprintf(filename,"TuningScanRow%02d.root",row);
	    else
	      sprintf(filename,"TuningScanMultipleRows.root");
	    char fileoption[200] = "";
	    if(update)
	      sprintf(fileoption,"update");
	    else
	      sprintf(fileoption,"recreate");
	    TH2D *myHisto = new TH2D(histoname, histoname, 64, -0.5, 63.5, nSteps, offset-step, offset+range+step);
	    std::vector<StreamData> toBeHistogrammed;
	    for(unsigned int i=0; i<nSteps; i++) {
	      resetPixels(0);
	      setThreshold((float) ((range/(nSteps-1)*i)+offset));
	      //std::cout << "Threshold step: " << std::dec << i << " Value is: " << (float) ((range/(nSteps-1)*i)+offset) << " Sleeping " << nsleep << "BCs" << std::endl;
	      unsigned int nHits=0;

	      std::vector<int> temp = captureCounters(nStrobes, 16, nsleep);
	      // And here is where the histogramming should happen over toBeHistogrammed[index]->getRawData(N[0-99])...
	      for(int index=0; index<64; index++) {
		myHisto->Fill((float) (index), (float) ((range/(nSteps-1)*i)+offset), temp[63-index]);
		nHits += temp[63-index];
	      }
	      //std::cout << " Hit Count was: " << nHits << std::endl;
	      toBeHistogrammed.clear();
	    }
	    //myHisto->Draw("COLZ");
	    //myHisto->SaveAs(filename);
	    TFile *myfile = new TFile(filename,fileoption);
	    myfile->cd();
	    myHisto->Write(myHisto->GetName(),TObject::kOverwrite);
	    myfile->Close();
	    std::cout << " Saved histo " << myHisto->GetName() << " to file " << myfile->GetName() << std::endl;
	    delete myHisto;
	    delete myfile;
	  }
	}
}

void TuningScanAllRows(unsigned int nSteps, float range, float offset, bool resetCalib, int nStrobes, unsigned int nsleep, ChipConfig *config, std::string nameoffile="", std::string nameofhisto="", bool singlerow = true, bool update = true){
  for(unsigned int r = 0; r<64;++r)
    TuningScanRow(nSteps, r, range, offset, resetCalib, nStrobes, nsleep, config, nameoffile, nameofhisto, singlerow, update);
}

void TuningScanMultipleRows(unsigned int row_max, unsigned int nSteps, float range, float offset, bool resetCalib, int nStrobes, unsigned int nsleep, ChipConfig *config, std::string nameoffile="", std::string nameofhisto="", bool singlerow = true, bool update = true){
  for(unsigned int r = 0; r<row_max;++r)
    TuningScanRow(nSteps, r, range, offset, resetCalib, nStrobes, nsleep, config, nameoffile, nameofhisto, singlerow, update);
}

void TuneConfigNeighborsRow(unsigned int nSteps, int nNeighbors, unsigned int row, float range, float offset, bool resetCalib, int nStrobes, unsigned int nsleep, ChipConfig *config, std::string nameoffile="",std::string nameofhisto="", bool singlerow = true, bool update = true, bool termoutput = false) {
	if (config == NULL) {config = new ChipConfig();}
	config->disable();
	if (row < 64) {
		config->enableRow(row);
	}
	std::vector<int> dac(64);
	std::vector<int> dacpol(64);
	std::vector<bool> polarity(64);
	for(unsigned int col = 0; col<64; col++){
	  // get DAC value of config per pixel
	  dac[col] = config->getColConfig(col)->getPixConfig(row)->getDAC(); 
	  polarity[col] = config->getColConfig(col)->getPixConfig(row)->isPositive();
	  dacpol[col] = (int(polarity[col])*2 -1 )*(dac[col]+0.5) + 15.5; // combine dac and pol to one int between 0 and 31
	  std::cout << "Col " << col << " Dac, pol: " << dac[col] << ", " << polarity[col]<< " DacPol: "<< dacpol[col] << std::endl;
	}
	// scan neighboring configurations 
	for(int neigh = - nNeighbors; neigh < nNeighbors+1; neigh++){ 
	  // loop over all cols to set all to the same DAC
	  for(unsigned int col = 0; col<64; col++){
	    int  pol, DAC;
	    int dacpolneigh; 
	    dacpolneigh = dacpol[col] + neigh; // scan neighboring configurations if not at edge
	    if(dacpolneigh < 0){ dacpolneigh = 0;}
	    else if(dacpolneigh > 31) {dacpolneigh = 31;}
	    pol = dacpolneigh/16; // extract polarity
	    DAC = int((dacpolneigh - 15.5)* (pol*2 - 1) - 0.5); // extract 4 bit DAC
	    //std::cout << "Col " << col << " Dac, pol: " << DAC << ", " << !!pol<< " DacPol: "<< dacpolneigh << std::endl;
	    setDACperPix(col, row, (unsigned int) DAC, !!pol, false, config); 
	  }
	  // FROM HERE SAME AS counterthresholdscanrow...
	  writeChip(config);
	  double step=(range/((double) nSteps-1))/2.0;
	  char histoname[200] = "";
	  if(!(nameofhisto==""))
	    sprintf(histoname,"%s",nameofhisto.c_str());
	  else 
	    sprintf(histoname,"TuningScanRow%02dNeigh%02d",row, neigh);
	  //sprintf(histoname,"TuningScanRow%02dDAC%02dPol%d",row, DAC, pol);
	  //char filename[200] = "";
	  //sprintf(filename,"TuningScanRow%02d.root",row);
	  char filename[200] = "";
	  if(!(nameoffile==""))
	    sprintf(filename,"%s",nameoffile.c_str());
	  else if(singlerow)
	    sprintf(filename,"TuningScanRow%02d.root",row);
	  else
	    sprintf(filename,"TuningScanMultipleRows.root");
	  char fileoption[200] = "";
	  if(update)
	    sprintf(fileoption,"update");
	  else
	    sprintf(fileoption,"recreate");
	  TH2D *myHisto = new TH2D(histoname, histoname, 64, -0.5, 63.5, nSteps, offset-step, offset+range+step);
	  std::vector<StreamData> toBeHistogrammed;
	  for(unsigned int i=0; i<nSteps; i++) {
	    resetPixels(0);
	    setThreshold((float) ((range/(nSteps-1)*i)+offset));
	    //std::cout << "Threshold step: " << std::dec << i << " Value is: " << (float) ((range/(nSteps-1)*i)+offset) << " Sleeping " << nsleep << "BCs" << std::endl;
	    unsigned int nHits=0;

	    std::vector<int> temp = captureCounters(nStrobes, 16, nsleep);
	    // And here is where the histogramming should happen over toBeHistogrammed[index]->getRawData(N[0-99])...
	    for(int index=0; index<64; index++) {
	      myHisto->Fill((float) (index), (float) ((range/(nSteps-1)*i)+offset), temp[63-index]);
	      nHits += temp[63-index];
	    }
	    //std::cout << " Hit Count was: " << nHits << std::endl;
	    toBeHistogrammed.clear();
	  }
	  //myHisto->Draw("COLZ");
	  //myHisto->SaveAs(filename);
	  TFile *myfile = new TFile(filename,fileoption);
	  myfile->cd();
	  myHisto->Write(myHisto->GetName(),TObject::kOverwrite);
	  myfile->Close();
	  std::cout << " Saved histo " << myHisto->GetName() << " to file " << myfile->GetName() << std::endl;
	  delete myHisto;
	  delete myfile;
	}
}

void TuneConfigNeighborsAllRows(unsigned int nSteps, int nNeighbors, float range, float offset, bool resetCalib, int nStrobes, unsigned int nsleep, ChipConfig *config, std::string nameoffile="", std::string nameofhisto="", bool singlerow = true, bool update = true){
  for(unsigned int r = 0; r<64;++r)
    TuneConfigNeighborsRow(nSteps, nNeighbors, r, range, offset, resetCalib, nStrobes, nsleep, config, nameoffile, nameofhisto, singlerow, update);
}



// This fct reads in the measurements of TuneConfigNeighborsAllRows().
// It optimizes/ checks the existing tuned DAC configuration.
// For each pixel the neigh neighbors relative to the optimal DAC config to both sides are scanned.
// The config with mean closest to the nominal value is chosen.
// The optimized DAC configurations are written out as a file that can be later read in to tune the chip with ReadConfig().
// Later, measurements and analysis can be combined in one fct.
void Retune(std::string rootfolder, int neigh, unsigned int yerror, float nom_mean, std::string fopti, bool header=true){
  TF1 *fgaus = new TF1("fGaus","gaus",1, 2);
  // read in initial dac configuration and store as value between 0 and 31 per pixel
  std::ifstream infile(fopti);
  std::string line;
  if(header)
    getline(infile, line); // skips header line
  int ro , colu, dac, pol;
  std::vector<vector<int>> dacpol( 64 , vector<int> (64));
  while(getline(infile, line)) {
    std::istringstream iss(line);
    iss >> ro >> colu >> dac >> pol;
    dacpol[ro][colu-1] = int((2*pol-1)*(dac +0.5) +15.5);
    //colu= 64 - colu; // naming convention is reversed
  }
  infile.close();
  std::ofstream outfile ("ReTunedconfig.txt");
  outfile <<  "Row Column DAC Polarity Constant Constant_err Mean Mean_err Sigma Sigma_err DACshift" << endl;

  int N = 2*neigh +1;
  int nrows = 64;
  for(unsigned int row = 0; row < 64; row++){
    cout << "Row: " << row << endl;
    char rfile[200] = "";
    sprintf(rfile,"%s/TuningScanRow%02d.root", rootfolder.c_str(), row);
    TFile *f = new TFile(rfile);
    vector<vector<float>> constants( nrows , vector<float> (N));
    vector<vector<float>> constants_err( nrows , vector<float> (N));
    vector<vector<float>> means( nrows , vector<float> (N));
    vector<vector<float>> means_err( nrows , vector<float> (N));
    vector<vector<float>> sigmas( nrows , vector<float> (N));
    vector<vector<float>> sigmas_err( nrows , vector<float> (N));
    vector<vector<float>> deviate( nrows , vector<float> (N));
    int index, newdacpol;

    for(int n = - neigh; n < neigh +1; n++){
      char histoname[200] = "";
      sprintf(histoname,"TuningScanRow%02dNeigh%02d",row,n);
      TH2D * histo = (TH2D*)f->Get(histoname);
      for(int col = 0; col < 64; col++){ // 0 to 64 to avoid overflow bins in y-projection
        TH1D *projy = histo->ProjectionY("histo_py",64 - col, 64- col); // naming convention is reversed in writing config to chip
        for(int b = 0 ; b < projy->GetNbinsX(); b++){ // set all errors to same value
                  projy->SetBinError(b, yerror); }
        projy->Fit("fGaus", "Q");
        constants[col][n + neigh] = fgaus->GetParameter(0);
        constants_err[col][n + neigh] = fgaus->GetParError(0);
        means[col][n + neigh] = fgaus->GetParameter(1);
        means_err[col][n + neigh] = fgaus->GetParError(1);
        sigmas[col][n + neigh] = fgaus->GetParameter(2);
        sigmas_err[col][n + neigh] = fgaus->GetParError(2);
        deviate[col][n + neigh] = abs(fgaus->GetParameter(1) - nom_mean);
      }
    }

    for(int i = 0; i < nrows; i++)
      {
        index = min_element(deviate[i].begin(), deviate[i].end()) - deviate[i].begin();
        //cout <<  row << " " << i << " " << index - neigh << " " << constants[i][index] << " " << means[i][index] << " "  << sigmas[i][index] << " " << endl;
        newdacpol = min(max(0, dacpol[row][i] + index - neigh ),31) ; // allow only config values inside of 0 to 31 range

        if (outfile.is_open()){
          outfile <<  row << " " << i+1 << " " <<  int((newdacpol -15.5)*(newdacpol/16 *2-1) -0.5) << " " <<  newdacpol/16 << " "  << constants[i][index] << " " << constants_err[i][index] << " " << means[i][index] << " " << means_err[i][index] << " "  << sigmas[i][index] << " " << sigmas_err[i][index] << " " << index - neigh << endl;
          }
        else
          cout << "unable to open outfile" << endl;
      }
  }
  outfile.close();
}

// Reads in DAC Config from file and stores in Chip Config. Later this can be written to he chip
void ReadConfig(string filename, ChipConfig *config, bool header = true, bool Colbitflipped = false) {
  if (config == NULL) {config = new ChipConfig();}
  
  std::ifstream infile(filename);
  std::string line;
  
  if(header)
    getline(infile, line); // skips header line
  unsigned int row , col, dac, pol;
  bool masked = false;
  while(getline(infile, line)) {
    std::istringstream iss(line);
    iss >> row >> col >> dac >> pol;
    bool polarity = !!pol; // for int to bool conversion
    col = 64 - col; // naming convention is reversed
    if(Colbitflipped) 
      col = ((col&2)==0) ? col+=2 : col-=2; // flipping second bit of col index
    setDACperPix(col, row, dac, polarity, masked, config); // column values in txt file range from 1 to 64, while row from 0 to 63. 
  }
}

// Variation of IcalibDac in IDACnSteps from (including) 0 to 100 uA. One Row is scanned through all 32 DAC Configurations.
void IcalibDAC(unsigned int IDACnSteps , unsigned int nSteps, unsigned int row, float range, float offset, bool resetCalib, int nStrobes, unsigned int nsleep, ChipConfig *config, std::string nameoffile="", std::string nameofhisto=""){
  setupAnalog();
  sleep(1); // wait
  for (unsigned int i=0; i< IDACnSteps+1; i++)
    { float IDACval = i*100./IDACnSteps; // includes 0 and 100 (in uA)   
      setIDAC(12, 0, IDACval);
      sleep(1);
      std::cout << "ICalibDAC value is: " <<   IDACval << " uA" << endl;
      char filename[200] = "";
      if(!(nameoffile==""))
	sprintf(filename,"IDAC%.0f%s",IDACval, nameoffile.c_str());
      else
	sprintf(filename,"TuningScanRow%02dIDAC%.0f.root",row,IDACval);
      TuningScanRow(nSteps, row, range, offset, resetCalib, nStrobes, nsleep, config, filename, nameofhisto);
    }
}


// Variation of current defined by i2c_addr and dacnum in given range. Current can be for example IShaperBias (12,1), IPreAmpBias (12,2) or IPFBPreAmp (13,2)
void CurrentVariation(unsigned int i2c_addr ,unsigned int dacnum, unsigned int ISteps, float Irange, float Ioffset, unsigned int nSteps, unsigned int row, float range, float offset, bool resetCalib, int nStrobes, unsigned int nsleep, ChipConfig *config, std::string nameoffile="", std::string nameofhisto=""){
  setupAnalog();
  sleep(1); // wait
  for (unsigned int i=0; i< ISteps+1; i++){
    float Ival = Ioffset + i*Irange/ISteps;
    setIDAC(i2c_addr, dacnum, Ival); // wait after changing current
    sleep(1); 
    std::cout << "Current set to " <<   Ival << " uA" << endl;
    char histoname[200] = "";
    if(!(nameofhisto==""))
      (histoname,"Ival%.0f%s",Ival, nameofhisto.c_str());
    else
      sprintf(histoname,"counterThresholdScanRow%02dIval%.0f", row, Ival);
    char filename[200] = "";
    if(!(nameoffile==""))
      (filename,"%s", nameoffile.c_str());
    else
      sprintf(filename,"counterThresholdScanRow%02dCurrentVariation.root", row);
    counterThresholdScanRow(nSteps, row, range, offset, resetCalib, nStrobes, nsleep, config, filename, histoname, false, true);
  }
}


void warmup(unsigned int minutes){
  ChipConfig* conf = new ChipConfig();
  ReadConfig("lucian_MA/220126_ReTunedConfig_75uA.txt", conf, true, true);
  for(unsigned int m = 0; m<minutes/2; m++){ // Scan takes around 2 minutes
    counterThresholdScanAllRows(61, 0.06, 1.14, true, 100000, 1100, conf, "warumUpScan.root");
    cout << m << " of " << minutes/2 <<  " Scans performed" ;}
}

/*
// THIS IS STILL UNCOMPLETED as datatype of config is problematic. Work in progress 
void SafeConfig(string filename, ChipConfig *config) {
  if (config == NULL) {config = new ChipConfig();}
  std::ofstream outfile(filename);
  for(unsigned int col = 0; col<64; col++){
    for(unsigned int row = 0; row<64; col++){
      unsigned char dac = config->getColConfig(col)->getPixConfig(row)->getDAC();
      int polarity =  config->getColConfig(col)->getPixConfig(row)->isPositive();
      int masked = config->getColConfig(col)->getPixConfig(row)->isMasked();
      outfile << row << col << dac << polarity << masked;
      }
    //int row , col, dac, polarity, masked;
  }
}
*/


// Fits gaussian to variation of DAC configs and writes fitresults to txt-file.
// The produced txt-file can then be used to tune each pixel by selecting the optimal dac configuration for a nominal threshold value.
// yerror is the error on the counts that is assumed via fitting and doesnt influence the position of the mean much.
// rootfolder is folder where .root files for each row from Retune() are saved.
void Fit_Rows_Cols_Tuning(string rootfolder, unsigned int yerror = 200){
  TF1 *fgaus = new TF1("fGaus","gaus",1, 2);
  ofstream outfile ("fitresults_tuning.txt");
  outfile <<  "Row Column DAC Polarity Constant Constant_err Mean Mean_err Sigma Sigma_err" << endl;
  for(int row = 0; row < 64; row++){ // 64
    cout << "Row: "<< row << endl;
    char rfile[200] = "";
    sprintf(rfile,"%s/TuningScanRow%02d.root", rootfolder.c_str(), row);
    TFile *f = new TFile(rfile);
    for(int pol = 0; pol < 2; pol++){
      for(int dac = 0; dac < 16; dac++){
        char histoname[200] = "";
        sprintf(histoname,"TuningScanRow%02dDAC%02dPol%01d",row,dac, pol);
        TH2D * histo = (TH2D*)f->Get(histoname);
        for(int col = 0; col < 64; col++){
          TH1D *projy = histo->ProjectionY("histo_py", col +1, col +1);// 1 to 64 to avoid overflow bins
          for(int b = 0 ; b < projy->GetNbinsX(); b++){ // set all errors to same value
                    projy->SetBinError(b, yerror); }
          projy->Fit("fGaus", "Q");
          outfile <<  row << " " << col + 1  << " " <<  dac << " "  << pol << " " <<  fgaus->GetParameter(0) << " " << fgaus->GetParError(0) << " "  << fgaus->GetParameter(1)<< " " << fgaus->GetParError(1)<< " " << fgaus->GetParameter(2)<< " " << fgaus->GetParError(2)<< endl;
        }
      }
    }
  }
  outfile.close();
}

// reads in txt file and selects the Dac configuration for each pixel with mean closest to nom_mean.
// txt input is produced via Fit_Rows_Cols_Tuning().
void Find_DAC_at_Thresh(string txtfile, float nom_mean, bool header = true ) {
  ifstream infile(txtfile);
  std::string line;
  if(header)
    getline(infile, line); // skips header line

  ofstream outfile ("optimDacs_tuning.txt");
  outfile <<  "Row Column DAC Polarity Constant Constant_err Mean Mean_err Sigma Sigma_err" << endl;
  int index, ro , colu, dac, pol, dacpol;
  float constant, constant_err, mean, mean_err, sigma, sigma_err;
  for(int row = 0; row < 64; row++){
    // for each row store 32 difference between mean and nominal value per pixel and find pixelwise their minimum
    vector<vector<float>> Deltameans( 64 , vector<float> (32));
    vector<vector<float>> Constant( 64 , vector<float> (32));
    vector<vector<float>> Constant_err( 64 , vector<float> (32));
    vector<vector<float>> Mean( 64 , vector<float> (32));
    vector<vector<float>> Mean_err( 64 , vector<float> (32));
    vector<vector<float>> Sigma( 64 , vector<float> (32));
    vector<vector<float>> Sigma_err( 64 , vector<float> (32));
    vector<vector<unsigned int>> Pol( 64 , vector<unsigned int> (32));
    vector<vector<unsigned int>> Dac( 64 , vector<unsigned int> (32));


    // read in 64 *32 lines for each row
    for(int count = 0; count < 64*32; count++){
      getline(infile, line);
      std::istringstream iss(line);
      iss >> ro >> colu >> dac >> pol >> constant >> constant_err >> mean >> mean_err >> sigma >> sigma_err;
      dacpol  = int((2*pol-1)*(dac +0.5) +15.5);
      Constant[colu-1][dacpol] = constant;
      Constant_err[colu-1][dacpol] = constant_err;
      Mean[colu-1][dacpol] = mean;
      Mean_err[colu-1][dacpol] = mean_err;
      Sigma[colu-1][dacpol] = sigma;
      Sigma_err[colu-1][dacpol] = sigma_err;
      Pol[colu-1][dacpol] = pol;
      Dac[colu-1][dacpol] = dac;
      Deltameans[colu-1][dacpol] = abs(mean- nom_mean);
    }
    // choose best mean for each pixel
    for(int col = 0; col < 64; col++){
    index = min_element(Deltameans[col].begin(), Deltameans[col].end()) - Deltameans[col].begin();
    outfile << row << " " << col +1  << " " << Dac[col][index]   << " " <<  Pol[col][index] << " " <<  Constant[col][index] << " " <<  Constant_err[col][index] << " " << Mean[col][index] << " " << Mean_err[col][index] << " " << Sigma[col][index]<< " " << Sigma_err[col][index]<< endl;
    }



  }
    infile.close();
}
