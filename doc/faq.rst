Frequently Asked Questions
==========================

startUP(…) won’t work
---------------------

So far this has typically been due to missing jumpers on your
motherboard or failing to switch on the powersupply - Check the jumper
configuration as per the following image:

.. figure:: img/decalMotherboard.jpg
   :alt: DECAL DAQ setup with NEXYS and Motherboard

   DECAL DAQ setup with NEXYS and Motherboard
