.. role:: raw-latex(raw)
   :format: latex
..

ITSDAQ Build Instructions
=========================


Windows
-------

**Do not use Windows directly, it's just a pain.** 

Prerequisites:

-  Download root_v5.34.34
-  Download http://landinghub.visualstudio.com/visual-cpp-build-tools –
   I’d currently recommend version 2015, but it’s really up to you
-  Download WinPCAP and the PCAP Developer Pack (unpack the developer
   pack into C::raw-latex:`\develop` )
-  Download Python (I prefer version 2, 32 bit edition - generally most
   compatible)
-  Install git-scm (comes with a bash for windows :-) )
-  Finally clone:
   https://gitlab.cern.ch/atlas-itk-strips-daq/itsdaq-sw.git

Easiest way to install ITSDAQ is through Python waf. To use that, open a
command shell and go to your itsdaq checkout. In there, run python with
parameters ``waf configure`` (add ``--msvc_targets="x86"`` in case
you’re on a modern windwows machine). Configure may fail due to a
problem with root 5: TTimeStamp.h needs to be modified in line 53 to
say:

.. code:: cpp

   #if defined(__CINT__) || (defined(R__WIN32) && (_MSC_FULL_VER < 190022816))

rather than:

.. code:: cpp

   #if defined(__CINT__) || (defined(R__WIN32)

At this point your configure should work (provided you set your ROOTSYS
as system variable) and you can now run python with ``waf build`` and
finally ``waf install``.

This compiles ITSDAQ to be ready to use. Standard directories required
by ITSDAQ are:

::

   C:\sctvar\config
   C:\sctvar\data
   C:\sctvar\ets
   C:\sctvar\ps
   C:\sctvar\results

Within config, you should also copy the *st_system_config.dat* from
`ITSDAQ\config` as well as the *default.det* - they won’t
harm… Might require masking out modules.

For running with UDP protocol, your DAQ should be set up as:

::

   DAQ udp 192.168.222.16 60001,60001

For further information related to ITSDAQ, please check the
`Strips Upgrade DAQ TWiki at CERN <https://twiki.cern.ch/twiki/bin/view/Atlas/StripsUpgradeDAQ>`__.



Easing the process on Windows
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Effectively: Use Linux! With Microsoft introducing the `Windows Subsystem for Linux <https://docs.microsoft.com/en-us/windows/wsl/about>`__, much of the compilation of ITSDAQ can be made (a lot) easier, but just running in a Ubuntu (Preferrably a late release with new gcc), installed as a subsystem on Windows 10. With hardware being communicated with through the network interface via UDP, no particular special permissions are required for ITSDAQ to operate.

An X-server can be used for graphical output. Without a strong opinion, I can recommend `VcXsrv <https://sourceforge.net/projects/vcxsrv/>`__. After starting it with the XLaunch command, set the display variable in your shell to localhost:0.0 and you're ready to see graphical output from root (provided you installed that).



Linux Build Instructions
------------------------

Preferentially one should run using ROOT 6
`c.f. remarks <remarks.md>`__. However all scripts should still operate
using ROOT 5.

Once both ROOT and BOOST are compiled and set up, ITSDAQ should compile
without further trouble. To use ITSDAQ, you need to create a var directory
with subdectories:

::

   config
   results
   ps
   etc
   data

An environment variable `SCTDAQ_DIR` needs setting, e.g. if you created
your var directory in `~/var`:

.. code:: bash

   export SCTDAQ_DIR=~/var

Once this is done, you need to generate a file st_system_config.dat in
`$SCTDAQ_DIR/config/`, following the example above this would be
`~/var/config/st_system_config.dat`.

The preferential mode to run ITSDAQ should be UDP based. To do so, your
`st_system_config.dat` should contain a DAQ line along the following:

::

   DAQ udp 192.168.222.16 60001,60001

All other lines in the file should be commented out, by either starting
with a `#` or a space

Mac OS X Instructions
---------------------

Mac OS X requires (at least in semi-recent editions of it) to run using
ROOT 6 `c.f. remarks <remarks.md>`__, as ROOT 5 has trouble
pre-processing the Mac OS ``stdint`` header file.

Once both ROOT and BOOST are compiled and set up, ITSDAQ should compile
without further trouble. To use ITSDAQ, you need to create a var directory
with subdectories:

::

   config
   results
   ps
   etc
   data

An environment variable `SCTDAQ_DIR` needs setting, e.g. if you created
your var directory in `~/var`:

.. code:: bash

   export SCTDAQ_DIR=~/var

Once this is done, you need to generate a file st_system_config.dat in
`$SCTDAQ_DIR/config/`, following the example above this would be
`~/var/config/st_system_config.dat`.

The preferential mode to run ITSDAQ should be UDP based. To do so, your
`st_system_config.dat` should contain a DAQ line along the following:

::

   DAQ udp 192.168.222.16 60001,60001

All other lines in the file should be commented out, by either starting
with a `#` or a space
