# README

This readme has initially been set up to provide help with getting the DECAL readout system and software to work. However, documentation with more details will, from now on, be made available at https://radecal-daq.web.cern.ch/.

# DECAL Data Acquisition Software

This Software is written to allow readout and testing of the DECAL test chip (photograph below). The software is based on [ITSDAQ](https://gitlab.cern.ch/atlas-itk-strips-daq/itsdaq-sw) and just adds functionality to allow running with a [NEXYS Video](https://reference.digilentinc.com/reference/programmable-logic/nexys-video/start) board and DECAL adapters, configuring these to operate the chip.

![Decal Chip Photo](doc/img/decalChip.jpg)

I am trying to keep it up to date by pushing regularly, but please ask jdopke_at_cern.ch, in case you feel like there is something amiss.

# Setup and Operation

For the initial software setup please see below (Build Instructions) whilst a setup of the scripts is described in [the Changelog](/CHANGELOG.md), as this depends on your software version.

_**Before following belows instructions please read them first (including Using the scripts) and prepare required software/cables/anything.**_

## Setting up the Hardware

DECAL Data Acquisition is done using a [NEXYS Video](https://reference.digilentinc.com/reference/programmable-logic/nexys-video/start) board from [Digilent](http://store.digilentinc.com/).

To set the board up for operation with DECAL, one needs to attach the Decal Motherboard prior to powering, as well as a low voltage supply (for NEXYS), a micro USB programming cable (or alternatively an SD card/USB stick) and finally a low voltage supply for the DECAL Motherboard (+6V as labelled on the board, supplied via screw terminals, see top right of the picture below)

![DECAL DAQ setup with NEXYS and Motherboard](doc/img/decalMotherboard.jpg)

Once this is set up, power up NEXYS as well as the 6V supply for the Decal motherboard. You can now program your NEXYS Video using [Adept](http://store.digilentinc.com/digilent-adept-2-download-only/). Find the latest Firmware, currently version 0xB1CC, on [Matt's Firmware Emporium](http://www.hep.ucl.ac.uk/~warren/upgrade/firmware/?C=M;O=D) and download it to the board. Adept should automatically detect the board once powered up. In case it doesn't your jumper configuration may well be incorrect. Please verify that jumpers (and switches) on Nexys are set as shown in the previous picture.

To start with do this all without plugging in the DECAL daughterboard (see below).

![DECAL Daughterboard](doc/img/decalDaughterboard.jpg)

![DECAL_FD Daughterboard slotted into Motherboard, Bias connector missing](doc/img/decal2Daughterboard.jpg)

Once confident that you can operate the motherboard (all operations but reading proper data back will work without the daughterboard), power down the 6V as well as NEXYS itself, insert your daughterboard (see image below for the vertical connector version) and rerun the power up sequence. 

![DECAL DAQ setup with NEXYS and Motherboard](doc/img/decalAssembly.jpg)

## Using the scripts

Use the `startUP()` function to get power up and set the chip up for initial operation. Up to 4 boolean parameters can be provided:
* Mode (false=PAD, true=Strip)
* PLL Enable (true=320Mbit/s outputs)
* Clear Test Vector (true means the test values at the top of column get reset to 0)
* Set a manual Phase adjustment, setting the phase to 0 initally

Once that is done you can read raw data from the outputs of the chip through:

`debugData(1, 18)` or `debugMe(1, 18[, true])` (the optional parameter in the latter orders the output by return channel number). This will print the returned data on the screen for you to examine. If you care for a more numerical approach, have a look at how `captureData` is used within `debugMe` to retrieve a vector of returned data.

The last two channels (when reading 18) are sampled versions of the clock delivered by the DECAL chip. As these are sampled at 640 MBit/s but only read with 320 MBit/s, the only reflect half of a 25ns readout window. (I.e. don't get confused that only one of them looks like a well-behaved clock)

Injecting numbers into the test vector will shift all numbers already in there further down to the following columns:  
- `inject_one()` - Injects **_1_** into a **_single_** column  
- `inject_zero()` - Injects **_0_** into a **_single_** column  
- `inject_number(value, nTimes)` - Injects **_value_** into a **_nTimes_** column(s), default for **_nTimes_** is one.

E.g.: `reset_testVector()` shifts in many zeros until the test vector is empty. `inject_one()` will feed a single one into the chip test vector. Repetetively `inject_zero()` will then push that one through all the columns till it disappears at the end of the test vector.

At the end of operation, **DO NOT FORGET** to power down the chip using `powerDOWN(0)`. The function takes a parameter, allowing to keep logic signals to the chip steady even if power is absent. However calling it with paramter _0_ is recommended.

If you feel courageous, have a look at [the tasklist](/CONTRIBUTING.md) and either pick something up or add/let me know (jdopke_at_cern.ch) whatever is missing.

## Tuning the Chip
Start up the chip via `pon()`.
Warm it up by performing threshold scans for around 10 minutes, like repetetively do: `counterThresholdScanAllRows(121, 0.2, 1.05, true, 100000, 1100, 0)` .

Now perform a threshold scan where each pixel is scanned for all 32 calibration DAC configurations. It is important that all peaks of the scan lie within the given range of (in this case) 1.05 and 1.31 V (1.05 + 0.26). If the chip behaves differently the range must be adjusted. This will take around 2 hours: `TuningScanAllRows(131, 0.26, 1.05, true, 100000, 1100, 0)`.
Safe the 64 root files - one for each row- in a folder.

Do gauss fits to all of the 64 x 64 x 32 peaks via `Fit_Rows_Cols_Tuning(rootfilefolder)`. A txt file that safes the fit parameters is produced.

Read in this txt-file and find configurations close to nominal mean value of the peaks (here 1.17 V) via `Find_DAC_at_Thresh(txtfilename, 1.17)`.
Another txt-file is safed containing the 64 x 64 optimal DAC configurations.

Finally, this file can be read in via
- `conf = new ChipConfig()`
- `ReadConfig("optimaltxtfile", conf)`.

In order to write it on the chip, pass `conf` as an argument to the threshold scans, like `counterThresholdScanAllRows(121, 0.2, 1.05, true, 100000, 1100, conf)`. Now the means of the scans should be tuned to the nominal value.

# Build Instructions

## Building ITSDAQ

Prerequisites:

- Download root_v5.34.34
- Download http://landinghub.visualstudio.com/visual-cpp-build-tools
-- I'd currently recommend version 2015, but it's really up to you
- Download WinPCAP and the PCAP Developer Pack (unpack the developer pack into C:\develop\ )
- Download Python (I prefer version 2, 32 bit edition - generally most compatible)
- Install git-scm (comes with a bash for windows :-) )
- Finally clone: https://gitlab.cern.ch/atlas-itk-strips-daq/itsdaq-sw.git

Easiest way to install ITSDAQ is through Python waf. To use that, open a command shell and go to your itsdaq checkout. In there, run python with parameters `waf configure` (add `--msvc_targets="x86"` in case you're on a modern windwows machine). Configure may fail due to a problem with root 5: TTimeStamp.h needs to be modified in line 53 to say:

```cpp
#if defined(__CINT__) || (defined(R__WIN32) && (_MSC_FULL_VER < 190022816))
```

rather than:

```cpp
#if defined(__CINT__) || (defined(R__WIN32)
```

At this point your configure should work (provided you set your ROOTSYS as system variable) and you can now run python with `waf build` and finally `waf install`.

This compiles ITSDAQ to be ready to use. Standard directories required by ITSDAQ are:
* C:\sctvar\config
* C:\sctvar\data
* C:\sctvar\ets
* C:\sctvar\ps
* C:\sctvar\results

Within config, you should also copy the _st_system_config.dat_ from _ITSDAQ\config_ as well as the _default.det_ - they won't harm... Might require masking out modules.

For running with UDP protocol, your DAQ should be set up as:

```
DAQ udp 192.168.222.16 60001,60001
```

For further information related to ITSDAQ, please check the Strips Upgrade DAQ TWiki at CERN:

https://twiki.cern.ch/twiki/bin/view/Atlas/StripsUpgradeDAQ



## Building/Installing DECAL Software for ITSDAQ

Copy files from subfolders into the corresponding subfolders in your ITSDAQ installation (DecalConfig.\* into stdll/, DecalMotherboard.\* and test_DECAL.cxx into macros/)

In ITSDAQ you'll need to modify stdll/linkdef.h to include (just before endif):

```cpp
#pragma link C++ class PixConfig;
#pragma link C++ class ColumnConfig;
#pragma link C++ class ChipConfig;
#pragma link C++ class RawData;
#pragma link C++ class StripHit;
#pragma link C++ class PadHit;
#pragma link C++ class RawDataDecoder;
#pragma link C++ class StreamData;
#pragma link C++ class std::vector<StripHit>;
#pragma link C++ class std::vector<PadHit>;
#pragma link C++ class std::vector<StreamData>;
#pragma link C++ class std::vector<unsigned int>;
```

Finally, to build DecalConfig Objects into stdll, you'll need to add DecalConfig to the compile parameters. This happens in wscript, located in your ITSDAQ base folder. In line 585 (roughly), it needs adding of `"DecalConfig.h",` to stdll_cls. Be sure to add DecalConfig.h before the last object in that variable, which always has to be `"linkdef.h"`.

Your stdll_cls should now look roughly like this:

```python
	stdll_cls = ["stdll/%s" % s for s in
							["TLogger.h", "TModuleDCS.h", "TModule.h", "../dcsdll/TDCSDevice.h",
							"TSequencer.h", "TSignals.h", "TST.h", "TChip.h", "HVStripV1.h",
							"DecalConfig.h", "linkdef.h"]]
```

A few lines below this entry you'll need to add DecalConfig to SrcNames, if possible before the line adding hsio (`if bld.env.use_hsio:`):

```python
	srcNames += ['DecalConfig'] # This line needs adding!
	if bld.env.use_hsio:
```

You can now re-run configuration/building/installation of ITSDAQ as done above and should be good to operate functions from DecalMotherboard.cpp and test_Decal.cxx in macros.

## Differences in running with ROOT 6

Rather than loading macros with headers through `.L <Macroname>.cpp` root6 needs to load dependencies. This is done through a dedicated function called `loadmacro("<Macroname>")`, which is maintained through ITSDAQ.
